package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/haisum/recaptcha"
)

func main() {
	// Parse command-line flags
	sitekeyPtr := flag.String("sitekey", "", "Site key - public key")
	secretkeyPtr := flag.String("secretkey", "", "Secret key - private key")
	filePtr := flag.String("file", "", "HTTP File to serve with SITEKEY for key")
	flag.Parse()

	if *sitekeyPtr == "" || *secretkeyPtr == "" || *filePtr == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	re := recaptcha.R{
		Secret: *secretkeyPtr,
	}
	sitekey := *sitekeyPtr

	data, err := ioutil.ReadFile(*filePtr)
	if err != nil {
		fmt.Printf("Erreur - impossible d'ouvrir le fichier %s : %s\n",
			*filePtr, err.Error())
		os.Exit(1)
	}
	htmlContent := strings.Replace(string(data), "SITEKEY", sitekey, -1)

	fmt.Printf("Starting recaptchatester : sitekey = %s, secretkey = %s, file = %s\n", sitekey, re.Secret, *filePtr)

	fmt.Println()

	/*	form := fmt.Sprintf(`
		<html>
			<head>
				<script src='https://www.google.com/recaptcha/api.js'></script>
			</head>
			<body>
				<form action="/submit" method="post">
					<div class="g-recaptcha" data-sitekey="%s"></div>
					<input type="submit">
				</form>
			</body>
		</html>
	`, sitekey)*/

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, htmlContent)
	})
	http.HandleFunc("/submit", func(w http.ResponseWriter, r *http.Request) {
		isValid := re.Verify(*r)
		if isValid {
			fmt.Fprintf(w, "Valid")
			log.Println("Valid")
		} else {
			fmt.Fprintf(w, "Invalid! These errors ocurred: %v", re.LastError())
			log.Printf("Invalid! These errors ocurred: %v", re.LastError())
		}
	})

	log.Printf("\n Starting server on http://localhost:8100 . Check example by opening this url in browser.\n")

	err = http.ListenAndServe(":8100", nil)

	if err != nil {
		log.Fatalf("Could not start server. %s", err)
	}
}
