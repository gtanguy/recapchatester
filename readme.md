recaptchatester - petite application de test de recaptcha

Pour le lancer : recapchatester -sitekey CLE_PUBLIQUE -secretkey CLE_PRIVEE -file fichier_HTML

Il suffit d'ouvrir la page http://localhost:8100

2 fichiers HTML sont proposés mais peuvent être modifiés (la clé publique est remplacée par SITEKEY) : pour le captcha invisible et pour le recaptcha normal

Pour builder soi-même, il faut télécharger (https://golang.org/dl) et installer (https://golang.org/doc/install) go, puis faire : go get gitlab.com/gtanguy/recapchatester
